<?php

define('SALT', 'wwhateveryouwant');
  

class HellopwTest extends \PHPUnit\Framework\TestCase {

	public function testPw() {

		$stack = [];
        $this->assertEquals(0, count($stack));

        array_push($stack, 'foo');
        $this->assertEquals('foo', $stack[count($stack)-1]);
        $this->assertEquals(1, count($stack));

        $this->assertEquals('foo', array_pop($stack));
        $this->assertEquals(0, count($stack));
	}

}

?>
